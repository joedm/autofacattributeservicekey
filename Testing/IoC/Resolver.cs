﻿using System;
using Autofac;
using Autofac.Features.AttributeFilters;
using AutofacMetaData.Bar;
using AutofacMetaData.Exctension;

namespace Testing.IoC
{
    // We don't need a pretty Autofac Integration for a basic test sample. This should do more than well enough.
    public class Resolver : IDisposable
    {
        private readonly ILifetimeScope _lifetimeScope;

        public Resolver()
        {
            _lifetimeScope = Container.BeginLifetimeScope();
        }

        public T Resolve<T>()
        {
            return _lifetimeScope.Resolve<T>();
        }

        public T ResolveKeyed<T>(string serviceKey)
        {
            return _lifetimeScope.ResolveKeyed<T>(serviceKey);
        }
        
        public bool IsRegistered<T>()
        {
            return _lifetimeScope.IsRegistered<T>();
        }

        public bool IsRegisteredWithKey<T>(string serviceKey)
        {
            return _lifetimeScope.IsRegisteredWithKey<T>(serviceKey);
        }

        private static readonly IContainer Container = BuildAutofacContainer();

        private static IContainer BuildAutofacContainer()
        {
            // In gCast we won't register them one by one, we'll auto register based on attributes and/or name suffix.
            Autofac.ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<FooBarService>()
                .AsKeyedImplementedInterfaces()
                .InstancePerLifetimeScope();

            containerBuilder.RegisterType<BarBarService>()
                .AsKeyedImplementedInterfaces()
                .InstancePerLifetimeScope();

            containerBuilder.RegisterType<ImSpecificallyDependantOnFooBarService>()
                .AsSelf()
                .WithAttributeFiltering()
                .InstancePerLifetimeScope();

            return containerBuilder.Build();
        }

        public void Dispose()
        {
            _lifetimeScope.Dispose();
        }
    }
}