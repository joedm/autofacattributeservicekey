﻿using System;
using AutofacMetaData.Bar;
using AutofacMetaData.Bar.Constants;
using Shouldly;
using Testing.IoC;
using Xunit;

namespace Testing
{
    public class AutofacResolverTests : IDisposable
    {
        private readonly Resolver _resolver;

        public AutofacResolverTests()
        {
            _resolver = new Resolver();
        }

        [Fact]
        public void Container_BarServiceIsRegistrationWithFooKey_True()
        {
            _resolver.IsRegisteredWithKey<IBarService>(ServiceKeys.Foo).ShouldBeTrue();
        }

        [Fact]
        public void Container_BarServiceIsRegistrationWithBarKey_True()
        {
            _resolver.IsRegisteredWithKey<IBarService>(ServiceKeys.Bar).ShouldBeTrue();
        }

        [Fact]
        public void Container_BarServiceIsRegistrationWithNoKey_False()
        {
            _resolver.IsRegistered<IBarService>().ShouldBeFalse();
        }

        [Fact]
        public void Container_ResolveFooKey_ReturnsFooBarType()
        {
            IBarService theFooService = _resolver.ResolveKeyed<IBarService>(ServiceKeys.Foo);
            theFooService.ShouldBeOfType<FooBarService>();
        }

        [Fact]
        public void Container_ResolveBarKey_ReturnsBarBarType()
        {
            IBarService theBarService = _resolver.ResolveKeyed<IBarService>(ServiceKeys.Bar);
            theBarService.ShouldBeOfType<BarBarService>();
        }

        [Fact]
        public void Container_ResolveWithoutKey_ThrowsException()
        {
            Should.Throw<Exception>(() => _resolver.Resolve<IBarService>());
        }

        [Fact]
        public void Container_ConstructorWithResolvableAttributeFilter_ResolvesCorrectDependency()
        {
            var constructorInjectionTest = _resolver.Resolve<ImSpecificallyDependantOnFooBarService>();
            constructorInjectionTest.TheFooBar.ShouldBeOfType<FooBarService>();
        }
        
        public void Dispose()
        {
            _resolver.Dispose();
        }
    }
}
