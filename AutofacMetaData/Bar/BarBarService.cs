﻿using AutofacMetaData.Attribute;
using AutofacMetaData.Bar.Constants;

namespace AutofacMetaData.Bar
{
    [AutofacServiceKey(ServiceKeys.Bar)]
    public class BarBarService : IBarService
    {
        public string Name { get; set; } = "Bar Bar Black Sheep";
    }
}