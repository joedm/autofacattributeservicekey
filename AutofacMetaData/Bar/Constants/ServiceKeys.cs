﻿namespace AutofacMetaData.Bar.Constants
{
    public class ServiceKeys
    {
        public const string Foo = "Foo";
        public const string Bar = "Bar";
        public const string Fake = "Fake"; // No implementations of IBarService actually use this key.
    }
}