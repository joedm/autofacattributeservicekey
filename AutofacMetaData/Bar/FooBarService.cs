﻿using AutofacMetaData.Attribute;
using AutofacMetaData.Bar.Constants;

namespace AutofacMetaData.Bar
{
    [AutofacServiceKey(ServiceKeys.Foo)]
    public class FooBarService : IBarService
    {
        public string Name { get; set; } = "Foo";
    }
}