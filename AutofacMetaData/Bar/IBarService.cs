﻿using AutofacMetaData.Attribute;

namespace AutofacMetaData.Bar
{
    [AutofacRegisterAsKeyedService]
    public interface IBarService
    {
        string Name { get; set; }
    }
}