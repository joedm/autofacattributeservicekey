﻿using Autofac.Features.AttributeFilters;
using AutofacMetaData.Bar.Constants;

namespace AutofacMetaData.Bar
{
    public class ImSpecificallyDependantOnFooBarService
    {
        public IBarService TheFooBar;
        
        public ImSpecificallyDependantOnFooBarService([KeyFilter(ServiceKeys.Foo)] IBarService theFooBar)
        {
            TheFooBar = theFooBar;
        }
    }
}