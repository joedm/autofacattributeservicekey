﻿using System;

namespace AutofacMetaData.Attribute
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class AutofacRegisterAsKeyedServiceAttribute : System.Attribute
    {
    }
}