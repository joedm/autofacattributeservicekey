﻿using System;

namespace AutofacMetaData.Attribute
{
    public class AutofacServiceKeyAttribute : System.Attribute
    {
        public string ServiceKey { get; private set; }

        public AutofacServiceKeyAttribute(string serviceKey)
        {
            ServiceKey = serviceKey;
        }
        
        public AutofacServiceKeyAttribute(Guid serviceKey)
        {
            ServiceKey = serviceKey.ToString("N");
        }
    }
}