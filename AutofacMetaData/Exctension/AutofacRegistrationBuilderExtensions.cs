﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac.Builder;
using AutofacMetaData.Attribute;

namespace AutofacMetaData.Exctension
{
    public static class AutofacRegistrationBuilderExtensions 
    {
        /// <summary>
        /// Specifies that a type is registered as providing all of its implemented interfaces.
        /// </summary>
        /// <typeparam name="TLimit">Registration limit type.</typeparam>
        /// <typeparam name="TConcreteActivatorData">Activator data type.</typeparam>
        /// <param name="registration">Registration to set service mapping on.</param>
        /// <returns>Registration builder allowing the registration to be configured.</returns>
        public static IRegistrationBuilder<TLimit, TConcreteActivatorData, SingleRegistrationStyle> AsKeyedImplementedInterfaces<TLimit, TConcreteActivatorData>(this IRegistrationBuilder<TLimit, TConcreteActivatorData, SingleRegistrationStyle> registration) where TConcreteActivatorData : IConcreteActivatorData
        {
            if (registration == null)
                throw new ArgumentNullException("registration");

            var attribute = typeof(TLimit).GetCustomAttribute(typeof(AutofacServiceKeyAttribute)) as AutofacServiceKeyAttribute;
            string serviceKey = attribute?.ServiceKey;

            if (string.IsNullOrEmpty(serviceKey))
                return registration; // No ServiceKey to Register, Just return without registering anything.

            Type[] keyedInterfaces = GetKeyedImplementedInterfaces(registration.ActivatorData.Activator.LimitType);

            foreach (Type keyedInterface in keyedInterfaces)
            {
                registration = registration.Keyed(serviceKey, keyedInterface);
            }

            return registration;
        }

        private static Type[] GetKeyedImplementedInterfaces(Type type)
        {
            IEnumerable<Type> types = type.GetTypeInfo().ImplementedInterfaces.Where(i => i.GetCustomAttribute(typeof(AutofacRegisterAsKeyedServiceAttribute)) != null);
            if (!type.GetTypeInfo().IsInterface)
                return types.ToArray<Type>();
            return types.Append<Type>(type).ToArray<Type>();
        }

        /// <summary>Appends the item to the specified sequence.</summary>
        /// <typeparam name="T">The type of element in the sequence.</typeparam>
        /// <param name="sequence">The sequence.</param>
        /// <param name="trailingItem">The trailing item.</param>
        /// <returns>The sequence with an item appended to the end.</returns>
        public static IEnumerable<T> Append<T>(this IEnumerable<T> sequence, T trailingItem)
        {
            if (sequence == null)
                throw new ArgumentNullException("sequence");
            foreach (T obj in sequence)
                yield return obj;
            yield return trailingItem;
        }
    }
}